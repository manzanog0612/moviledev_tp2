﻿using UnityEngine;
using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi;


public class PlayGames : MonoBehaviour
{
    public int playerScore;
    string leaderboardID = "";

    static string[] achievementID = new string[6];

    public static PlayGames instance = null;
    public static PlayGamesPlatform platform;

    private void Awake()
    {
        if (!instance)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
            Initialize();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Initialize()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        achievementID[0] = GPGSIds.achievement_starting_up;
        achievementID[1] = GPGSIds.achievement_going_foward;
        achievementID[2] = GPGSIds.achievement_half_way_there;
        achievementID[3] = GPGSIds.achievement_never_gonna_give_you_up;
        achievementID[4] = GPGSIds.achievement_commitment;
        achievementID[5] = GPGSIds.achievement_compulsive_buyer;

        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            platform = PlayGamesPlatform.Activate();
        }

        Social.Active.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Logged in successfully");
            }
            else
            {
                Debug.Log("Login Failed");
                //PluginsManager.SendLog("Login Failed");
            }
        });
#endif
    }

    public void AddScoreToLeaderboard()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (Social.Active.localUser.authenticated)
        {
            Social.ReportScore(playerScore, leaderboardID, success => { });
        }
#endif
    }

    public void ShowLeaderboard()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (Social.Active.localUser.authenticated)
        {
            platform.ShowLeaderboardUI();
        }
#endif
    }

    public void ShowAchievements()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (Social.Active.localUser.authenticated)
        {
            platform.ShowAchievementsUI();
        }
#endif
    }

    public static void UnlockAchievement(int archivementId)
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (Social.Active.localUser.authenticated)
        {
            Social.ReportProgress(achievementID[archivementId], 100f, success => { });
        }
#endif
    }
}
