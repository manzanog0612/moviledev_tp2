﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Generics.TriggerObject;

namespace Game.Controllers
{
    public class CCActions
    {
        //public Action<Action> OnMoveCamera = null;
        public Action<float> OnSetDistance = null;
    }

    public class CameraController : MonoBehaviour
    {
        #region PRIVATE_FIELDS
        private float distance = 0f;
        //private IEnumerator goDownInst = null;
        private Vector3 initialPosition = Vector3.zero;
        private Camera mainCamera = null;
        private TriggerObject2D cameraTrigger = null;
        #endregion

        #region EXPOSED_FIELDS
        [SerializeField] private float speed = 2f;
        #endregion

        #region ACTIONS
        private CCActions ccActions = new CCActions();
        #endregion

        #region PROPERTIES
        public TriggerObject2D CameraTrigger => cameraTrigger;
        #endregion

        #region INIT
        public void Init()
        {
            mainCamera = Camera.main;

            initialPosition = mainCamera.transform.position;
            cameraTrigger = mainCamera.gameObject.GetComponentInChildren<TriggerObject2D>();

            //ccActions.OnMoveCamera += MoveDown;
            ccActions.OnSetDistance += SetDistance;
        }
        #endregion

        #region PUBLIC METHODS
        public CCActions GetActions()
        {
            return ccActions;
        }

        public void SetTriggerAction(Action callFirst, Action callBack)
        {
            cameraTrigger.OnActivatedTrigger = () => { MoveDown(callFirst, callBack); };
        }
            
        public void Reset()
        {
            mainCamera.transform.position = initialPosition;
            cameraTrigger.OnActivatedTrigger = null;
        }

        public void SetDistance(float distance)
        {
            this.distance = distance;
        }
        #endregion

        #region PRIVATE_METHODS
        private void MoveDown(Action callFirst, Action callBack)
        {
            IEnumerator GetDown()
            {
                Vector3 finalPos = new Vector3(0f, mainCamera.transform.position.y - distance, -10);
                Vector3 initialPos = mainCamera.transform.position;
                float time = 0f;

                callFirst?.Invoke();

                while (time < 1f)
                {
                    time += Time.deltaTime * speed;
                    mainCamera.transform.position = Vector3.Lerp(initialPos, finalPos, time);
                    yield return null;
                }

                mainCamera.transform.position = finalPos;

                callBack?.Invoke();
            }

            StartCoroutine(GetDown());
        }
        #endregion
    }
}