﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Entities;
using Game.Data;
using Game.Controllers;

namespace Game.Controllers
{
    public class LevelGenerator : MonoBehaviour
    {
        #region PRIVATE_FIELDS
        #endregion

        #region EXPOSED_FIELDS
        [SerializeField] private GameObject winningLine = null;
        [SerializeField] private float sceenHeight = 0f;
        #endregion

        #region ACTIONS
        private PCActions pcActions = null;
        private CActions cActions = null;
        private CCActions ccAction = null;
        #endregion

        #region PROPERTIES
        #endregion

        #region INIT
        public void InitModuleHandlers(PCActions pcActions, CActions cActions, CCActions ccAction)
        {
            this.pcActions = pcActions;
            this.cActions = cActions;
            this.ccAction = ccAction;
        }
        #endregion

        #region PUBLIC_METHODS
        public void ConfigureLevel(LevelData levelData)
        {
            int amoutGroups = levelData.PlatformGroupData.Length;
            pcActions.OnSetPlatformGroupsData?.Invoke(levelData.PlatformGroupData, levelData.Speed, levelData.DistanceBetweenPlatforms, levelData.StartYPos);
            cActions.OnSetData(levelData.DistanceBetweenPlatforms, Vector3.up * (levelData.StartYPos + levelData.DistanceBetweenPlatforms / 2f));
            ccAction.OnSetDistance?.Invoke(levelData.CameraNeedsToMove ? sceenHeight : 0f);

            Vector3 winningLinePosition = pcActions.OnGetLastGroupPosition() - Vector3.up * (levelData.DistanceBetweenPlatforms / 2f);
            winningLinePosition.x = 0;

            winningLine.transform.position = winningLinePosition;
        }

        public Vector3 GetWinningLinePosition()
        {
            return winningLine.transform.position;
        }
        #endregion
    }
}
