﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Generics.PoolSystem;
using Game.Entities;
using Game.Data;

namespace Game.Controllers
{
    public class PCActions
    {
        public Action<PlatformGroupData[], float, float, float> OnSetPlatformGroupsData = null;
        public Func<Vector3> OnGetLastGroupPosition = null;
    }

    public class PlatformsController : PoolObjectsManager
    {
        #region PRIVATE_FIELDS
        private List<PlatformGroup> platformGroups = new List<PlatformGroup>();
        #endregion

        #region EXPOSED_FIELDS
        [Header("Platform Groups Configuration")]
        private float zPos = 0f;
        #endregion

        #region ACTIONS
        private PCActions pcActions = new PCActions();
        #endregion

        #region INIT
        public void Init()
        {
            InitPool();

            pcActions.OnSetPlatformGroupsData += SetPlatformGroupsData;
            pcActions.OnGetLastGroupPosition += GetLastGroupPosition;
        }
        #endregion

        #region PUBLIC_METHODS
        public PCActions GetActions()
        {
            return pcActions;
        }

        public void SetPlatformGroupsData(PlatformGroupData[] platformGroupData, float speed, float distance, float startYPos)
        {            
            for (int i = 0; i < platformGroupData.Length; i++)
            {
                Vector3 startPos = new Vector3(platformGroupData[i].startXPos, startYPos - distance * i, zPos);

                PlatformGroup platformGroup = ActivateObject().GetComponent<PlatformGroup>();

                platformGroup.Init();
                platformGroup.SetPlatforms(platformGroupData[i].distance, startPos, speed, platformGroupData[i].movementType);
                
                platformGroups.Add(platformGroup);
            }
        }

        public void MoveGroups()
        {
            for (int i = 0; i < platformGroups.Count; i++)
            {
                platformGroups[i].MoveGroup();
            }
        }

        public void ClearData()
        {
            for (int i = 0; i < platformGroups.Count; i++)
            {
                platformGroups[i].ClearData();
            }

            platformGroups.Clear();
            DeactivateAll();
        }
        #endregion

        #region PRIVATE_METHODS
        private Vector3 GetLastGroupPosition()
        {
            return platformGroups[platformGroups.Count - 1].transform.position;
        }
        #endregion
    }
}