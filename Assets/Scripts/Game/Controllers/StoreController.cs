﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controllers
{
    [System.Serializable]
    public class Skin
    {
        public int price = 0;
        public bool unlocked = false;
        public Sprite sprite = null;
        public bool selected = false;
    }

    [System.Serializable]
    public class StoreData
    {
        public static StoreData instance = null;
        
        public StoreData()
        {
            skins = new Skin[amountSkins];
        }

        private int amountSkins = 4;
        public Skin[] skins = null;

        public int AmountSkins => amountSkins; 
    }

    public class StoreController : MonoBehaviour
    {
        [System.Serializable]
        public class StoreItem
        {
            public GameObject[] unlockedObjects = null;
            public GameObject[] lockedObjects = null;
            public Text priceText = null;
        }

        #region PRIVATE_FIELDS
        [SerializeField] private int coinsAvaiable = 0;
        #endregion

        #region EXPOSED_FIELDS
        [SerializeField] private Skin[] skins = null;
        [SerializeField] private StoreItem[] storeItem = null;
        [SerializeField] private Image selectedQuad = null;
        #endregion

        #region ACTIONS
        private Action<Sprite> onSelected = null;
        private Action<int> onPurchased = null;
        private Action onPurchasedAllskins = null;
        #endregion

        #region INIT
        public void Configure(Action<Sprite> onSelected, Action<int> onPurchased, Action onPurchasedAllskins, int coins)
        {
            this.onSelected = onSelected;
            this.onPurchased = onPurchased;
            this.onPurchasedAllskins = onPurchasedAllskins;
            coinsAvaiable = coins;
        }
        #endregion

        #region UNITY_CALL
        private void OnEnable()
        {
            if (StoreData.instance == null)
            {
                StoreData.instance = new StoreData();

                for (int i = 0; i < StoreData.instance.AmountSkins; i++)
                {
                    StoreData.instance.skins[i] = skins[i];
                }
            }
            else
            {
                for (int i = 0; i < StoreData.instance.AmountSkins; i++)
                {
                    skins[i] = StoreData.instance.skins[i];
                }
            }

            for (int i = 1; i < skins.Length; i++)
            {
                storeItem[i].priceText.text = "x" + skins[i].price.ToString();
                SetLocked(storeItem[i], !skins[i].unlocked);

                if (skins[i].selected)
                {
                    selectedQuad.transform.position = storeItem[i].unlockedObjects[0].transform.position;
                }
            }

            if (AreAllSkinsUnlocked())
            {
                onPurchasedAllskins?.Invoke();
            }
        }
        #endregion

        #region PRIVATE_METHODS
        private void SetLocked(StoreItem storeItem, bool locked)
        {
            for (int i = 0; i < storeItem.unlockedObjects.Length; i++)
            {
                storeItem.unlockedObjects[i].SetActive(!locked);
            }

            for (int i = 0; i < storeItem.lockedObjects.Length; i++)
            {
                storeItem.lockedObjects[i].SetActive(locked);
            }
        }

        private void UpdateSkins()
        {
            for (int i = 0; i < skins.Length; i++)
            {
                StoreData.instance.skins[i] = skins[i];
            }
        }

        private bool AreAllSkinsUnlocked()
        {
            for (int i = 0; i < skins.Length; i++)
            {
                if (!skins[i].unlocked)
                {
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region PUBLIC_METHODS
        public void SetSelected(int skinId)
        {
            if (!skins[skinId].unlocked)
            {
                if (coinsAvaiable >= skins[skinId].price)
                {
                    onPurchased?.Invoke(skins[skinId].price);
                    coinsAvaiable -= skins[skinId].price;
                    skins[skinId].unlocked = true;

                    SetLocked(storeItem[skinId], !skins[skinId].unlocked);

                    PluginsManager.SendLog("Skin " + skinId.ToString() + " purchased");

                    if (AreAllSkinsUnlocked())
                    {
                        onPurchasedAllskins?.Invoke();
                    }
                }
            }

            if (skins[skinId].unlocked)
            {
                for (int i = 0; i < skins.Length; i++)
                {
                    skins[skinId].selected = false;
                }

                selectedQuad.transform.position = storeItem[skinId].unlockedObjects[0].transform.position;
                onSelected?.Invoke(skins[skinId].sprite);
                skins[skinId].selected = true;

                UpdateSkins();
            }
        }
        #endregion
    }
}