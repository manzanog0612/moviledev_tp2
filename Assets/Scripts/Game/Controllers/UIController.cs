﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controllers
{
    public class UIActions
    {
        public Action OnGoodResult = null;
        public Action OnBadResult = null;
        public Action OnPause = null;
        public Action OnResume = null;
    }

    public class UIController : MonoBehaviour
    {
        #region EXPOSED_FIELDS
        [Header("UI Configuration")]
        [SerializeField] private GameObject[] pausePanelElements = null;
        [SerializeField] private GameObject[] retryPanelElements = null;
        [SerializeField] private GameObject[] winPanelElements = null;
        [SerializeField] private GameObject[] finalPanelElements = null;
        [SerializeField] private GameObject gameplayPanel = null;
        [SerializeField] private GameObject[] resultText = null;
        [SerializeField] private Text rocketCoinstText = null;
        [SerializeField] private Text time = null;
        #endregion

        #region ACTIONS
        private UIActions uiActions = new UIActions();
        private Action<bool> onSetPause = null;
        private Action onEndLevel = null;
        #endregion

        #region INIT
        public void Init()
        {
            uiActions.OnGoodResult += OnGoodResult;
            uiActions.OnBadResult += OnBadResult;
            uiActions.OnPause += OnPause;
            uiActions.OnResume += OnResume;
        }

        public void InitModuleHandlers(Action<bool> onSetPause, Action onEndLevel)
        {
            this.onSetPause = onSetPause;
            this.onEndLevel = onEndLevel;
        }
        #endregion

        #region PUBLIC_METHODS
        public UIActions GetActions()
        {
            return uiActions;
        }

        public void UpdateUI(float time)
        {
            this.time.text = FormatTime(time);
        }

        public void EndGame()
        {
            SetAllElementsOff();
            SetUIElementsActive(ref finalPanelElements, true);
        }

        public void EndLevel()
        {
            onEndLevel?.Invoke();
        }

        public void OnGoodResult()
        {
            SetAllElementsOff();
            SetUIElementsActive(ref winPanelElements, true);
            SetUIElementsActive(ref resultText, true);
            SetResultText("YOU WON!");
        }

        public void OnBadResult()
        {
            SetAllElementsOff();
            SetUIElementsActive(ref retryPanelElements, true);
            SetUIElementsActive(ref resultText, true);
            SetResultText("YOU LOST!");
        }

        public void OnPause()
        {
            onSetPause?.Invoke(true);
            SetAllElementsOff();
            SetUIElementsActive(ref pausePanelElements, true);
        }

        public void OnResume()
        {
            onSetPause?.Invoke(false);
            SetAllElementsOff();
            gameplayPanel.SetActive(true);
        }

        public void SetRocketCoinsNumber(int rocketCoins)
        {
            rocketCoinstText.text = "x" + rocketCoins.ToString();
        }
        #endregion

        #region PRIVATE
        private void SetUIElementsActive(ref GameObject[] elements, bool active)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                elements[i].SetActive(active);
            }
        }

        private void SetResultText(string text)
        {
            resultText[0].GetComponent<Text>().text = text;
        }

        private void SetAllElementsOff()
        {
            SetUIElementsActive(ref retryPanelElements, false);
            SetUIElementsActive(ref winPanelElements, false);
            SetUIElementsActive(ref resultText, false);
            SetUIElementsActive(ref pausePanelElements, false);
            gameplayPanel.SetActive(false);
        }

        private string FormatTime(float fTime)
        {
            TimeSpan t = TimeSpan.FromSeconds(fTime);

            return string.Format("{1:D2}:{2:D2}:{3:D2}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds);
        }
        #endregion
    }
}

