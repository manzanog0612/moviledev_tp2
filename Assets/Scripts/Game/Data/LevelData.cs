﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    public enum MOVEMENT_TYPE { NORMAL, RANDOM_PATRON1, RANDOM_PATRON2, RANDOM_PATRON3 }

    [System.Serializable]
    public struct PlatformGroupData
    {
        public MOVEMENT_TYPE movementType;
        public float distance;
        public float startXPos;
    }

    [CreateAssetMenu(fileName = "LevelData_", menuName = "LevelData")]
    public class LevelData : ScriptableObject
    {
        #region EXPOSED_FIELDS
        [Header("Level Configuration")]
        [SerializeField] private PlatformGroupData[] platformGroupData = null;
        [SerializeField] private float distanceBetweenPlatforms = 0f;
        [SerializeField] private float startYPos = 0f;
        [SerializeField] private float speed = 0f;
        [SerializeField] private bool cameraNeedsToMove = false;
        [SerializeField] private float[] rocketTime = null; 
        #endregion

        #region PROPERTIES
        public PlatformGroupData[] PlatformGroupData => platformGroupData;
        public float DistanceBetweenPlatforms => distanceBetweenPlatforms; 
        public float StartYPos => startYPos;
        public float Speed => speed;
        public bool CameraNeedsToMove => cameraNeedsToMove;
        public float[] RocketTime => rocketTime;
        #endregion
    }
}