﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.InputManagement;
using Game.Generics.Audio;

namespace Game.Entities
{
    public class CActions
    {
        public Action<float, Vector3> OnSetData = null;
        public Action OnCollided = null;
        public Action OnTriggered = null;
    }

    public class Character : MonoBehaviour
    {
        #region PRIVATE_FIELDS
        private Rigidbody2D body = null;
        private SpriteRenderer spriteRenderer = null;
        private IEnumerator goDownInst = null;
        private float distance = 0f;
        #endregion

        #region EXPOSED_FIELDS
        [Header("Character Configurations")]
        [SerializeField] private float downSpeed = 5f;
        [SerializeField] private float sideSpeed = 5f;
        #endregion

        #region ACTIONS
        private CActions cActions = new CActions();
        private AudioActions audioActions = null;
        #endregion

        #region PROPERTIES
        public SpriteRenderer SpriteRenderer => spriteRenderer;
        #endregion

        #region INIT
        public void Init()
        {
            body = GetComponent<Rigidbody2D>();
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            cActions.OnSetData += SetData;
        }

        public void InitModuleHandlers(AudioActions audioActions, Action OnCollided, Action OnTriggered)
        {
            this.audioActions = audioActions;
            cActions.OnCollided += OnCollided;
            cActions.OnTriggered += OnTriggered;
        }
        #endregion

        #region UNITY_CALLS
        private void OnCollisionEnter2D(Collision2D collision)
        {
            cActions.OnCollided?.Invoke();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            cActions.OnTriggered?.Invoke();
        }
        #endregion

        #region PUBLIC_METHODS
        public CActions GetActions()
        {
            return cActions;
        }

        public void ClearData()
        {
            transform.position = Vector3.zero;
            distance = 0f;

            if (goDownInst != null)
            {
                StopCoroutine(goDownInst);
            }
        }

        public void SetSprite(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;
        }

        public void GoDown()
        {
            if (goDownInst != null)
            {
                return;
            }

            IEnumerator GetDown()
            {
                Vector3 finalPos = new Vector3(transform.position.x, transform.position.y - distance, 0f);
                Vector3 initialPos = transform.position;
                float time = 0f;

                while (time < 1f)
                {
                    time += Time.deltaTime * sideSpeed;
                    transform.position = Vector3.Lerp(initialPos, finalPos, time);
                    yield return null;
                }

                transform.position = finalPos;

                goDownInst = null;
            }

            goDownInst = GetDown();
            StartCoroutine(goDownInst);

            audioActions.OnGoDown?.Invoke();
        }

        public void AddAngularVelocity(float velocity)
        {
            body.AddForce(Vector2.zero + Vector2.right * velocity * sideSpeed);
        }
        #endregion

        #region PRIVATE_METHODS
        private void SetData(float distance, Vector3 startPos)
        {
            transform.position = startPos;
            this.distance = distance;
        }
        #endregion
    }
}