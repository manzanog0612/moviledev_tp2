﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Entities
{
    public class Platform : MonoBehaviour
    {
        #region PRIVATE_FIELDS
        private float width = 0f;
        #endregion

        #region EXPOSED_FIELDS
        [Header("Configuration")]
        [SerializeField] private Transform leftEnd = null;
        [SerializeField] private Transform rightEnd = null;
        #endregion

        #region PROPERTIES
        public Transform LeftEnd => leftEnd; 
        public Transform RightEnd => rightEnd;
        public float Width => width; 
        #endregion

        #region INIT
        public void Init()
        {
            width = rightEnd.transform.position.x - leftEnd.transform.position.x;
        }
        #endregion

        #region PUBLIC_METHODS
        #endregion
    }
}
