﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Data;

namespace Game.Entities
{
    public class PlatformGroup : MonoBehaviour
    {
        #region PRIVATE_FIELDS
        private float distance = 0f;
        private float speed = 0f;
        private Vector3 direction = Vector3.zero;
        private Platform[] platforms = null;
        private MOVEMENT_TYPE movementType = MOVEMENT_TYPE.NORMAL;
        private List<Vector3> positionChangePoints = new List<Vector3>();
        private int actualPositionPoint = 0;
        #endregion

        #region EXPOSED_FIELDS
        [SerializeField] private float halfScreenSize = 2.8f; 
        #endregion

        #region INIT
        public void Init()
        {
            platforms = GetComponentsInChildren<Platform>();

            for (int i = 0; i < platforms.Length; i++)
            {
                platforms[i].Init();
            }
        }
        #endregion

        #region PUBLIC_METHODS
        public void SetPlatforms(float distance, Vector3 startPos, float speed, MOVEMENT_TYPE movementType)
        {
            this.distance = distance;
            this.speed = speed;
            
            this.movementType = movementType;
            transform.position = startPos;
            
            float totalGroupLenght = (platforms.Length - 1) * distance;

            for (int i = 1; i < platforms.Length + 1; i++)
            {
                totalGroupLenght += platforms[i - 1].Width;
            }

            Vector3 initialPosition = startPos;
            initialPosition += Vector3.left * totalGroupLenght / 2;

            platforms[0].transform.position = initialPosition + Vector3.right * platforms[0].Width / 2;

            for (int i = 1; i < platforms.Length; i++)
            {
                Vector3 position = platforms[i - 1].RightEnd.position + Vector3.right * (distance + platforms[i].Width / 2);
                platforms[i].transform.position = position;
            }

            SetMovementType(movementType);

            direction = actualPositionPoint % 2 == 0 ? Vector3.left : Vector3.right;
        }

        public void MoveGroup()
        {
            transform.position += direction * speed * Time.deltaTime;

            UpdateDirection();
        }

        public void ClearData()
        {
            distance = 0f;
            speed = 0f;
            direction = Vector3.zero;
            movementType = MOVEMENT_TYPE.NORMAL;
            actualPositionPoint = 0;

            positionChangePoints.Clear();
        }
        #endregion

        #region PRIVATE_METHODS
        private void SetMovementType(MOVEMENT_TYPE movementType)
        {
            positionChangePoints.Clear();

            switch (movementType)
            {
                case MOVEMENT_TYPE.NORMAL:

                    positionChangePoints = new List<Vector3>() { new Vector3(-halfScreenSize, 0, 0), 
                                                                 new Vector3(halfScreenSize, 0, 0) };

                    break;

                case MOVEMENT_TYPE.RANDOM_PATRON1:

                    positionChangePoints = new List<Vector3>(){ new Vector3(-halfScreenSize, 0, 0), 
                                                                new Vector3(halfScreenSize, 0, 0), 
                                                                new Vector3(0, 0, 0) };

                    break;
                case MOVEMENT_TYPE.RANDOM_PATRON2:

                    positionChangePoints = new List<Vector3>(){ new Vector3(-halfScreenSize / 2f, 0, 0),
                                                                new Vector3(halfScreenSize, 0, 0),
                                                                new Vector3(-halfScreenSize, 0, 0),
                                                                new Vector3(halfScreenSize / 2f, 0, 0) };

                    break;
                case MOVEMENT_TYPE.RANDOM_PATRON3:

                    positionChangePoints = new List<Vector3>(){ new Vector3(-halfScreenSize / 2f, 0, 0),
                                                                new Vector3(halfScreenSize, 0, 0),
                                                                new Vector3(-halfScreenSize / 2f, 0, 0),
                                                                new Vector3(0, 0, 0),
                                                                new Vector3(-halfScreenSize, 0, 0),
                                                                new Vector3(halfScreenSize / 2f, 0, 0) };

                    break;
                default:
                    break;
            }

            actualPositionPoint = Random.Range(0, positionChangePoints.Count);
        }

        private void UpdateDirection()
        {
            bool checkMinor = direction == Vector3.left ? true : false;
            bool changeDirection;

            if (checkMinor)
            {
                changeDirection = transform.position.x < positionChangePoints[actualPositionPoint].x;
            }
            else
            {
                changeDirection = transform.position.x > positionChangePoints[actualPositionPoint].x;
            }

            if (changeDirection)
            {
                direction = -direction;

                actualPositionPoint = actualPositionPoint == positionChangePoints.Count - 1 ? 0 : actualPositionPoint + 1;
            }
        }
        #endregion
    }
}