﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Managers;
using Game.Controllers;
using Game.Data;
using Game.Entities;
using Game.InputManagement;
using Game.Generics.Audio;

namespace Game.Logic
{
    public class GameLogic : MonoBehaviour
    {
        const int amountStart = 3;

        #region PRIVATE_FIELDS
        private LevelData currentLevelData = null;

        private float winningYPos = 0f;
        private float[] rocketTime = new float[amountStart];
        private float time = 0f;
        private int rocketCoins = 0;
        #endregion

        #region EXPOSED_FIELDS
        [Header("Main Configuration")]
        [SerializeField] private LevelGenerator levelGenerator = null;
        [SerializeField] private PlatformsController platformsController = null;
        [SerializeField] private Character character = null;
        [SerializeField] private CameraController cameraController = null;
        #endregion

        #region ACTIONS
        private Action onLevelEnd = null;
        private Action onGameEnd = null;
        private Action<bool> onShowResult = null;
        private Action<int> onSetLevelByIndex = null;
        private Action<int> onRocketCoinsChanged = null;

        private InputActions inputActions = null;
        private AudioActions audioActions = null;
        #endregion

        #region PROPERTIES
        public float Time => time;
        public int RocketCoins => rocketCoins;
        #endregion

        #region INIT
        public void InitGame()
        {
            platformsController.Init();
            character.Init();
            cameraController.Init();

            rocketTime = new float[amountStart];

            onSetLevelByIndex(0);
        }

        public void InitModuleHandlers(InputActions inputActions, AudioActions audioActions)
        {
            levelGenerator.InitModuleHandlers(platformsController.GetActions(), character.GetActions(), cameraController.GetActions());
            character.InitModuleHandlers(audioActions, () => 
            { 
                onShowResult?.Invoke(false);
                cameraController.CameraTrigger.SetEnable(false);
            }, 
            null);

            this.inputActions = inputActions;
        }
        #endregion

        #region PUBLIC_METHODS
        public void Configure(Action onLevelEnd, Action<bool> onShowResult, Action<int> onSetLevelByIndex, Action<int> onRocketCoinsChanged, int rocketCoins)
        {
            this.onLevelEnd = onLevelEnd;
            this.onShowResult = onShowResult;
            this.onSetLevelByIndex = onSetLevelByIndex;
            this.onRocketCoinsChanged = onRocketCoinsChanged;

            this.rocketCoins = rocketCoins;
        }

        public void SetLevel(LevelData currentLevelData)
        {
            this.currentLevelData = currentLevelData;
        }

        public void ResetLevel()
        {
            levelGenerator.ConfigureLevel(currentLevelData);

            winningYPos = levelGenerator.GetWinningLinePosition().y + 1f;

            if (currentLevelData.CameraNeedsToMove)
            {
                cameraController.SetTriggerAction(() =>
                {
                    inputActions.OnSetEnable?.Invoke(false);
                },
                () =>
                {
                    inputActions.OnSetEnable?.Invoke(true);
                });

                cameraController.CameraTrigger.SetEnable(true);
            }
            else
            {
                cameraController.CameraTrigger.SetEnable(false);
            }

            rocketTime = currentLevelData.RocketTime;
            time = 0f;
        }

        public void UpdateGame()
        {
            time += UnityEngine.Time.deltaTime;

            platformsController.MoveGroups();

            if (inputActions.OnMoveDown())
            {
                character.GoDown();
            }

            character.AddAngularVelocity(inputActions.OnMoveSideways());

            if (CheckPlayerWin())
            {
                SetRocketCoins();
                onShowResult?.Invoke(true);
                cameraController.CameraTrigger.SetEnable(false);
            }
        }

        public void EndLevel()
        {
            platformsController.ClearData();
            character.ClearData();
            cameraController.Reset();

            onLevelEnd?.Invoke();
        }
        #endregion

        #region PRIVATE_METHODS
        private bool CheckPlayerWin()
        {
            return character.transform.position.y <= winningYPos;
        }

        private void SetRocketCoins()
        {
            for (int i = 0; i < rocketTime.Length; i++)
            {
                if (rocketTime[i] > time)
                {
                    rocketCoins++;
                }
            }

            onRocketCoinsChanged?.Invoke(rocketCoins);
        }
        #endregion
    }
}
