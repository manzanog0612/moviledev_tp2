﻿using Game.Controllers;
using Game.InputManagement;
using Game.Logic;
using Game.Entities;
using UnityEngine;
using Game.Generics.LoaderScreen;
using Game.Generics.Audio;
using UnityEngine.SceneManagement;

namespace Game.Managers
{
    public class GameManager : MonoBehaviour
    {
        #region SINGLETON
        private static GameManager instance = null;
        public static GameManager Instance => instance;
        #endregion

        #region PRIVATE_FIELDS
        private bool paused = false;
        private Vector2[] velocity = null;
        private Rigidbody2D[] allBodies = null;

        private Sprite playerSprite = null;
        private int coins = 0;
        #endregion

        #region EXPOSED_FIELDS
        [Header("Managers")]
        [SerializeField] private LevelManager levelManager = null;
        [SerializeField] private UIController uiController = null;
        [SerializeField] private InputManager inputManager = null;
        [SerializeField] private GameLogic gameLogic = null;
        #endregion

        #region UNITY_CALLS
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Start()
        {
            LoaderManager.Get().Init(PrepareScene);
        }

        private void PrepareScene()
        {
            if (SceneManager.GetActiveScene().name == "Game")
            {
                UILevelLoader.Get().SetVisible(true);
                Screen.orientation = ScreenOrientation.Portrait;

                levelManager = FindObjectOfType<LevelManager>();
                inputManager = FindObjectOfType<InputManager>();
                uiController = FindObjectOfType<UIController>();
                gameLogic = FindObjectOfType<GameLogic>();

                levelManager.Init();
                inputManager.Init();
                uiController.Init();

                paused = false;

                InitGame();
            }
            else if (SceneManager.GetActiveScene().name == "Store")
            {
                FindObjectOfType<StoreController>().Configure((sprite) => { playerSprite = sprite; }, 
                                                              (rocketCoins) => { coins -= rocketCoins; }, 
                                                              () => { PlayGames.UnlockAchievement(5); }, coins); 
            }
        }

        private void Update()
        {
            if (SceneManager.GetActiveScene().name == "Game")
            {
                uiController?.UpdateUI(gameLogic.Time);

                if (paused)
                {
                    return;
                }

                gameLogic?.UpdateGame();
            }
        }
        #endregion

        #region PUBLIC_METHODS
        public void SetLevel(int level)
        {
            levelManager.SetProblem(level);
            gameLogic.SetLevel(levelManager.GetCurrentLevel());
            gameLogic.ResetLevel();
            PluginsManager.SendLog("Level " + level + " set");
        }

        public void EndLevel()
        {
            if (levelManager.GetCurrentLevelIndex() > 0)
            { 
                PlayGames.UnlockAchievement(levelManager.GetCurrentLevelIndex() - 1);
            }

            if (levelManager.SolvedAllLevels())
            {
                EndGame();
            }
            else
            {
                SetLevel(levelManager.GetCurrentLevelIndex());
                uiController.OnResume();

                for (int i = 0; i < velocity.Length; i++)
                {
                    velocity[i] = Vector2.zero;
                }

                PauseGame(false);
            }
        }
        #endregion

        #region PRIVATE_METHODS
        private void PauseGame(bool pause)
        {
            paused = pause;

            if (paused)
            {
                StopAllCoroutines();
            }

            EnableAllBodies(!paused);
        }

        private void InitGame()
        {
            allBodies = FindObjectsOfType<Rigidbody2D>();
            velocity = new Vector2[allBodies.Length];

            uiController.InitModuleHandlers(PauseGame, EndLevel);
            uiController.SetRocketCoinsNumber(coins);

            AudioManager.Get().Init();

            gameLogic.Configure(EndLevel, ShowResult, SetLevel, (rocketCoins) => { coins = rocketCoins; }, coins);
            gameLogic.InitModuleHandlers(inputManager.GetActions(), AudioManager.Get().GetActions());
            gameLogic.InitGame();

            if (playerSprite)
            {
                FindObjectOfType<Character>().SpriteRenderer.sprite = playerSprite;
            }

            PluginsManager.SendLog("Start Game");
        }

        private void ShowResult(bool result)
        {
            uiController.SetRocketCoinsNumber(gameLogic.RocketCoins);

            if (result)
            {
                PluginsManager.SendLog("Level " + levelManager.GetCurrentLevelIndex() + " won");
                AudioManager.Get().OnLevelWin();
                uiController.GetActions().OnGoodResult();
            }
            else
            {
                PluginsManager.SendLog("Level " + levelManager.GetCurrentLevelIndex() + " lost");
                AudioManager.Get().OnDeath();
                uiController.GetActions().OnBadResult();
            }

            PauseGame(true);
        }

        private void EndGame()
        {
            AudioManager.Get().OnGameWin();
            inputManager.SetEnable(false);
            uiController.EndGame();

            PluginsManager.SendLog("End Game");
        }

        private void EnableAllBodies(bool enable)
        {
            for (int i = 0; i < allBodies.Length; i++)
            {
                if (enable)
                {
                    allBodies[i].velocity = velocity[i];
                }
                else
                {
                    velocity[i] = allBodies[i].velocity;
                    allBodies[i].velocity = Vector2.zero;
                }
            }
        }
        #endregion
    }
}