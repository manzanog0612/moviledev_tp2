﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Game.InputManagement
{
    public class InputActions
    {
        public Func<bool> OnMoveDown = null;
        public Func<float> OnMoveSideways = null;
        public Action<bool> OnSetEnable = null;
    }

    public class InputManager : MonoBehaviour
    {
        [SerializeField] Text text = null;

        #region PRIVATE_FIELDS
        private bool tapDettected = false;
        #endregion

        #region ACTIONS
        private InputActions inputActions = new InputActions();
        #endregion

        #region INIT
        public void Init()
        {
            inputActions.OnMoveDown += GetDownInput;
            inputActions.OnSetEnable += SetEnable;
            inputActions.OnMoveSideways += GetDeviceRotation;

            if (SystemInfo.supportsGyroscope)
            {
                Input.gyro.enabled = true;
                Input.gyro.updateInterval = 1.1167f;
            }

            PluginsManager.SendLog("System supports gyroscope: " + SystemInfo.supportsGyroscope.ToString());
        }
        #endregion

        #region PUBLIC_METHODS
        public InputActions GetActions()
        {
            return inputActions;
        }

        public void SetEnable(bool enable)
        {
            PluginsManager.SendLog("Input enabled: " + enable.ToString());

            enabled = enable;
        }
        #endregion

        #region PRIVATE_METHODS
        private bool GetDownInput()
        {
#if UNITY_ANDROID && !UNITY_EDITOR

            tapDettected = false;

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId) && touch.phase == TouchPhase.Began)
                {
                    tapDettected = true;
                }
            }

#elif UNITY_EDITOR

            if (Input.GetMouseButtonDown(0))
            {
                tapDettected = true;
            }
            else
            {
                tapDettected = false;
            }
#endif

            return tapDettected; 
        }

        private float GetDeviceRotation()
        {
            if (SystemInfo.supportsGyroscope)
            {
                text.text = Input.acceleration.ToString();
                return Input.acceleration.x;
            }
            else
            {
                text.text = "no giroscope";
                return 0;
            }
        }
#endregion
    }
}

