﻿using System;
using UnityEngine;

using Game.Data;

namespace Game.Managers
{
    public class LevelManager : MonoBehaviour
    {
        #region PRIVATE_FIELDS
        private int levelIndex = 0;
        private int totalLevels = 0;
        #endregion

        #region EXPOSED_FIELDS
        [SerializeField] private LevelData[] levels = null;
        #endregion

        #region INITIALIZATION
        public void Init()
        {
            totalLevels = levels.Length;
        }
        #endregion

        #region PUBLIC_METHODS
        public void FinishedProblem()
        { 
            levelIndex++;
        }
        public void SetProblem(int level)
        {
            levelIndex = level;
        }

        public bool SolvedAllLevels()
        {
            return levelIndex >= levels.Length ? true : false;
        }

        public LevelData GetLevelByIndex(int index)
        {
            return levels[index];
        }

        public LevelData GetCurrentLevel()
        {
            return levels[levelIndex];
        }

        public int GetCurrentLevelIndex()
        {
            return levelIndex;
        }

        public int GetTotalProblems()
        {
            return totalLevels;
        }
        #endregion
    }
}
