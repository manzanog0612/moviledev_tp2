﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Game.Generics.LoaderScreen;
using Game.Generics.Audio;

namespace Game.Managers
{
    public class MenuManager : MonoBehaviour
    {
        #region EXPOSED_FIELDS
        [SerializeField] private GameObject[] mainMenuObjects = null;
        [SerializeField] private GameObject[] logsPanelObjects = null;
        [SerializeField] private GameObject[] rulesPanelObjects = null;
        [SerializeField] private GameObject[] optiosPanel = null;
        [SerializeField] private Text logsText = null;
        #endregion

        #region PUBLIC_METHODS
        public void GoToGame()
        {
            LoaderManager.Get().LoadScene("Game");
        }

        public void GoToStore()
        {
            LoaderManager.Get().LoadScene("Store");
        }

        public void GoToRules()
        {
            SetObjectsActive(ref mainMenuObjects, false);
            SetObjectsActive(ref rulesPanelObjects, true);
        }

        public void GoToOptions()
        {
            SetObjectsActive(ref mainMenuObjects, false);
            SetObjectsActive(ref optiosPanel, true);
        }

        public void GoToMainMenu()
        {
            if (SceneManager.GetActiveScene().name == "MainMenu")
            {
                SetObjectsActive(ref logsPanelObjects, false);
                SetObjectsActive(ref rulesPanelObjects, false);
                SetObjectsActive(ref optiosPanel, false);
                SetObjectsActive(ref mainMenuObjects, true);
            }
            else
            {
                LoaderManager.Get().LoadScene("MainMenu");
            }
        }

        public void GoToLogsPanel()
        {
            SetObjectsActive(ref mainMenuObjects, false);
            SetObjectsActive(ref logsPanelObjects, true);

            logsText.text = PluginsManager.ReadFile();
        }

        public void OnClick()
        {
            AudioManager.Get().OnClickButton();
        }

        public void OnFastClick()
        {
            AudioManager.Get().OnFastClickButton();
        }

        public void AskIfClearLogs()
        {
            PluginsManager.ShowAlertDialog(new string[] { "Alert", "You are about to erase all game logs.", "Confirm", "Cancel" }, (a) =>
            {
                PluginsManager.ClearLogs();
                logsText.text = PluginsManager.ReadFile();
            }, (a) =>
            {
                PluginsManager.SendLog("Operation canceled");
            });
        }

        public void ShowArchivements()
        {
            PlayGames.instance.ShowAchievements();
        }

        public void ExitGame()
        {
            Application.Quit();
        }
        #endregion

        #region PRIVATE_METHODS
        private void SetObjectsActive(ref GameObject[] objects, bool active)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                objects[i].SetActive(active);
            }
        }
        #endregion
    }
}