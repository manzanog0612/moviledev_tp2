﻿using UnityEngine;
using System.Collections;
public class LogOutputHandler : MonoBehaviour
{
    public static LogOutputHandler Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }
    public void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    public void HandleLog(string logString, string stackTrace, LogType type)
    {
        string parameters = "";
        parameters += "Level=" + WWW.EscapeURL(type.ToString());
        parameters += "&";
        parameters += "Message=" + WWW.EscapeURL(logString);
        parameters += "&";
        parameters += "Stack_Trace=" + WWW.EscapeURL(stackTrace);
        parameters += "&";
        parameters += "Device_Model=" + WWW.EscapeURL(SystemInfo.deviceModel);

        PluginsManager.SendLog(parameters);
    }
}
