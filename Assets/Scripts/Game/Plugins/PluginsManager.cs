﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PluginsManager
{
    const string PACK_NAME = "com.example.myapplication";

    const string LOGGER_CLASS_NAME = "Logger";
    const string ALERTVIEWCALLER_CLASS_NAME = "AlertViewCaller";
    const string FILEMANAGER_CLASS_NAME = "FileManager";

    class AlertViewCallback : AndroidJavaProxy
    {
        private System.Action<int> alertHandler;

        public AlertViewCallback(System.Action<int> alertHandlerIn) : base(PACK_NAME + "." + ALERTVIEWCALLER_CLASS_NAME + "$AlertViewCallback")
        {
            alertHandler = alertHandlerIn;
        }
        public void onButtonTapped(int index)
        {
            Debug.Log("Button tapped: " + index);
            if (alertHandler != null)
                alertHandler(index);
        }
    }

    static AndroidJavaClass LoggerClass = null;
    static AndroidJavaClass AlertViewCallerClass = null;
    static AndroidJavaClass FileManagerClass = null;
    static AndroidJavaObject LoggerInstace = null;
    static AndroidJavaObject AlertViewCallerInstance = null;

    static void Init()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        LoggerClass = new AndroidJavaClass(PACK_NAME + "." + LOGGER_CLASS_NAME);
        AlertViewCallerClass = new AndroidJavaClass(PACK_NAME + "." + ALERTVIEWCALLER_CLASS_NAME);
        FileManagerClass = new AndroidJavaClass(PACK_NAME + "." + FILEMANAGER_CLASS_NAME);
        
        AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");

        FileManagerClass.SetStatic<AndroidJavaObject>("mainActivity", activity);
        AlertViewCallerClass.SetStatic<AndroidJavaObject>("mainActivity", activity);
        FileManagerClass.SetStatic<AndroidJavaObject>("mainActivity", activity);
        

        LoggerInstace = LoggerClass.CallStatic<AndroidJavaObject>("GetInstance");
        AlertViewCallerInstance = AlertViewCallerClass.CallStatic<AndroidJavaObject>("GetInstance");
#endif
    }

    public static void SendLog(string msg)
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (LoggerInstace == null)
        {
            Init();
        }

        LoggerInstace.Call("SendLog", msg);
        WriteToFile(msg);
#endif
    }

    public static void ShowAlertDialog(string[] strings, System.Action<int> handlerPositive = null, System.Action<int> handlerNegative = null)
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (AlertViewCallerInstance == null)
        {
            Init();
        }

        if (strings.Length<3)
       {
           Debug.LogError("AlertView requires at least 3 strings");
           return;
       }
       
       if (Application.platform == RuntimePlatform.Android)
            AlertViewCallerInstance.Call("ShowAlertView", new object[] { strings, new AlertViewCallback(handlerPositive), new AlertViewCallback(handlerNegative)});
       else
           Debug.LogWarning("AlertView not supported on this platform");
#endif
    }

    public static void WriteToFile(string msg)
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        if (FileManagerClass == null)
        {
            Init();
        }

        FileManagerClass.CallStatic("WriteToFile", msg, Application.persistentDataPath);
#endif
    }

    public static string ReadFile()
    {
        string fileText;

#if !UNITY_EDITOR && UNITY_ANDROID
        if (FileManagerClass == null)
        {
            Init();
        }

        string a = "";
        fileText = FileManagerClass.CallStatic<string>("ReadFile", a);
#else
        fileText = "no file";
#endif
        return fileText;

    }

    public static void ClearLogs()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        string a = "";
        FileManagerClass.CallStatic("ClearFile", Application.persistentDataPath);
#endif
    }
}
