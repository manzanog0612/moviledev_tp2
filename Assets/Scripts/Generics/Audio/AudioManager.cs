﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game.Generics.Audio
{
    public class AudioActions
    {
        public Action OnGoDown = null;
        public Action OnDeath = null;
        public Action OnLevelWin = null;
        public Action OnGameWin = null;
    }

    public class AudioManager : MonoBehaviourSingleton<AudioManager>
    {
        #region PRIVATE_FIELDS
        private AudioSource sceneAS = null;
        private AudioSource fastClickAS = null;
        private AudioSource clickAS = null;
        private AudioSource deathAS = null;
        private AudioSource levelWinAS = null;
        private AudioSource gameWinAS = null;
        private AudioSource downAS = null;
        private float musicVolume = 0.7f;
        private float soundsVolume = 1f;
        #endregion

        #region EXPOSED_FIELDS
        [SerializeField] private string sceneASTag = "";
        [SerializeField] private string fastClickASTag = "";
        [SerializeField] private string clickASTag = "";
        [SerializeField] private string deathASTag = "";
        [SerializeField] private string levelWinASTag = "";
        [SerializeField] private string gameWinASTag = "";
        [SerializeField] private string downASTag = "";
        #endregion

        #region ACTIONS
        private AudioActions amActions = new AudioActions();
        #endregion

        #region UNITY_CALLS
        public override void Awake()
        {
            base.Awake();
            FindAllAudioSources();
            OnChangedScene("MainMenu");
        }

        public void Update()
        {
            if (SceneManager.GetActiveScene().name == "MainMenu")
            {
                if (sceneAS && fastClickAS && clickAS)
                {
                    ConfigureVolume();
                    SetVolume();
                }
            }
        }
        #endregion

        #region INIT
        public void Init()
        {
            amActions.OnDeath = OnDeath;
            amActions.OnGoDown = OnGoDown;
            amActions.OnLevelWin = OnLevelWin;
            amActions.OnGameWin = OnGameWin;
        }
        #endregion

        #region PUBLIC_METHODS
        public AudioActions GetActions()
        {
            return amActions;
        }

        public void OnChangedScene(string nameScene)
        {
            FindAllAudioSources();

            if (nameScene == "MainMenu" || nameScene == "Store")
            {
                sceneAS.clip = Resources.Load("MenuTheme") as AudioClip;
            }
            else if (nameScene == "Game")
            {
                sceneAS.clip = Resources.Load("GameTheme") as AudioClip;
            }

            if (fastClickAS)
                fastClickAS.clip = Resources.Load("FastClick") as AudioClip;
            if (clickAS)
                clickAS.clip = Resources.Load("Click") as AudioClip;
            if (downAS)
                downAS.clip = Resources.Load("Down") as AudioClip;
            if (deathAS)
                deathAS.clip = Resources.Load("Death") as AudioClip;
            if (levelWinAS)
                levelWinAS.clip = Resources.Load("LevelWin") as AudioClip;
            if (gameWinAS)
                gameWinAS.clip = Resources.Load("GameWin") as AudioClip;

            SetVolume();

            sceneAS.Play();
        }

        public void OnFastClickButton()
        {
            fastClickAS.Play();
        }

        public void OnClickButton()
        {
            clickAS.Play();
        }

        public void OnGoDown()
        {
            downAS.Play();
        }

        public void OnDeath()
        {
            deathAS.Play();
        }

        public void OnLevelWin()
        {
            levelWinAS.Play();
        }

        public void OnGameWin()
        {
            gameWinAS.Play();
        }
        #endregion

        #region PRIVATE_METHODS
        private AudioSource FindAudioSourceWithTag(string tag)
        {
            AudioSource[] sources = FindObjectsOfType<AudioSource>();

            for (int i = 0; i < sources.Length; i++)
            {
                if (sources[i].tag == tag)
                {
                    return sources[i];
                }
            }

            return null;
        }

        private void FindAllAudioSources()
        {
            sceneAS = FindAudioSourceWithTag(sceneASTag);
            fastClickAS = FindAudioSourceWithTag(fastClickASTag);
            clickAS = FindAudioSourceWithTag(clickASTag);
            deathAS = FindAudioSourceWithTag(deathASTag);
            downAS = FindAudioSourceWithTag(downASTag);
            levelWinAS = FindAudioSourceWithTag(levelWinASTag);
            gameWinAS = FindAudioSourceWithTag(gameWinASTag);
        }

        private void ConfigureVolume()
        {
            Slider[] volumeSliders = FindObjectsOfType<Slider>();

            for (int i = 0; i < volumeSliders.Length; i++)
            {
                if (volumeSliders[i].tag == "SoundsVolume")
                {
                    soundsVolume = volumeSliders[i].value;
                }
                else if (volumeSliders[i].tag == "MusicVolume")
                {
                    musicVolume = volumeSliders[i].value;
                }
            }
        }

        private void SetVolume()
        {
            sceneAS.volume = musicVolume;

            if (fastClickAS)
                fastClickAS.volume = soundsVolume;
            if (clickAS)
                clickAS.volume = soundsVolume;
            if (deathAS)
                deathAS.volume = soundsVolume;
            if (downAS)
                downAS.volume = soundsVolume;
            if (levelWinAS)
                levelWinAS.volume = soundsVolume;
            if (gameWinAS)
                gameWinAS.volume = soundsVolume;
        }
        #endregion
    }
}