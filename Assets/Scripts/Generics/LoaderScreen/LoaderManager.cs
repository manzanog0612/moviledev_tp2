﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Game.Generics.Audio;

namespace Game.Generics.LoaderScreen
{
    public class LoaderManager : MonoBehaviourSingleton<LoaderManager>
    {
        #region EXPOSED_FIELDS
        [SerializeField] float minimumTime = 2;
        #endregion

        #region PRIVATE_FIELDS
        public float loadingProgress = 0;
        public float timeLoading;
        #endregion

        #region ACTIONS
        private Action onChangeScene = null;
        #endregion

        #region INIT
        public void Init(Action onChangeScene)
        {
            this.onChangeScene = onChangeScene;
        }
        #endregion

        #region PUBLIC_METHODS
        public void LoadScene(string scene)
        {
            UILevelLoader.Get().SetVisible(true);
            StartCoroutine(LoadAssets(scene));
        }
        #endregion

        #region PRIVATE_METHODS
        private IEnumerator LoadAssets(string scene)
        {
            loadingProgress = 0;
            timeLoading = 0;
            yield return null;

            AsyncOperation operation = SceneManager.LoadSceneAsync(scene);
            operation.allowSceneActivation = false;

            while (!operation.isDone)
            {
                timeLoading += Time.deltaTime;
                loadingProgress = operation.progress + 0.1f;
                loadingProgress = loadingProgress * timeLoading / minimumTime;

                if (loadingProgress >= 1)
                {
                    operation.allowSceneActivation = true;
                }

                yield return null;
            }

            UILevelLoader.Get().SetVisible(false);
            AudioManager.Get().OnChangedScene(scene);

            onChangeScene?.Invoke();
        }
        #endregion
    }
}
