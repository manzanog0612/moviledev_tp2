﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Generics.LoaderScreen
{
    public class UILevelLoader : MonoBehaviourSingleton<UILevelLoader>
    {
        public Image loadingImage;

        public override void Awake()
        {
            base.Awake();
            gameObject.SetActive(false);
        }

        public void SetVisible(bool show)
        {
            gameObject.SetActive(show);
        }

        public void Update()
        {
            float loadingVal = LoaderManager.Get().loadingProgress;
            Vector3 rotation = Vector3.zero;
            rotation.z = loadingVal;

            loadingImage.transform.Rotate(rotation); // rotacion de la imagen de carga

            if (LoaderManager.Get().loadingProgress >= 1)
                SetVisible(false);
        }
    }
}