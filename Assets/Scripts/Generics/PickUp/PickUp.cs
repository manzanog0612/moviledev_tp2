﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Game.Entities;

namespace Generics.PickUps
{
    public class PickUp : MonoBehaviour
    {
        protected Game.Entities.Character player = null;
        protected float totalDurability = 5f;
        protected float leftDurability = 5f;
        protected bool picked = false;
        protected bool consumed = false;

        public Action<GameObject> OnConsumed = null;

        public Game.Entities.Character Player { set => player = value; }
        public bool Picked { get => picked; }

        protected virtual void Update()
        {
            if (consumed)
            {
                return;
            }

            if (picked)
            {
                if (leftDurability < 0)
                {
                    consumed = true;
                    OnConsumed?.Invoke(gameObject);
                }

                leftDurability -= Time.deltaTime;
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            OnPicked();
        }

        protected virtual void OnPicked()
        {
            picked = true;
        }

        public virtual void ResetStats()
        {
            leftDurability = totalDurability;
            picked = false;
            consumed = false;
        }
    }
}           
