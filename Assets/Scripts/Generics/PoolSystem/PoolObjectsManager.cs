﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generics.PoolSystem
{
    public class PoolObjectsManager : MonoBehaviour
    {
        #region EXPOSED_FIELDS
        [Header("Pool system")]
        [SerializeField] private GameObject[] objects = null;
        [SerializeField] private Queue<GameObject> objectsPool = null;
        #endregion

        #region PROPERTIES
        public GameObject[] Objects => objects;
        #endregion

        #region INIT
        public void InitPool()
        {
            objectsPool = new Queue<GameObject>();

            for (int i = 0; i < objects.Length; i++)
            {
                objectsPool.Enqueue(objects[i]);
            }
        }
        #endregion

        #region PUBLIC_METHODS
        public void DeactivateObject(GameObject gObject)
        {
            gObject.SetActive(false);
            objectsPool.Enqueue(gObject);
        }

        public GameObject ActivateObject()
        {
            GameObject gObject = objectsPool.Dequeue();
            int index = 0;
            bool noObjectToReturn = false;

            while (gObject.activeSelf && !noObjectToReturn)
            {
                objectsPool.Enqueue(gObject);
                gObject = objectsPool.Dequeue();

                index++;

                if (index > objectsPool.Count)
                {
                    noObjectToReturn = true;
                    objectsPool.Enqueue(gObject);
                }
            }

            if (!noObjectToReturn)
            {
                gObject.SetActive(true);
                return gObject;
            }
            else
            {
                Debug.LogError("No objects available to activate!");
                return null;
            }
        }

        public void DeactivateAll()
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i].activeSelf)
                {
                    DeactivateObject(objects[i]);
                }
            }
        }
        #endregion
    }
}

