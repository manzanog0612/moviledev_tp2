﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Generics.TriggerObject
{
    public class TriggerObject2D : MonoBehaviour
    {
        public Action OnActivatedTrigger = null;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!enabled)
            {
                return;
            }

            OnActivatedTrigger?.Invoke();
        }

        public void SetEnable(bool enable)
        {
            enabled = enable;
        }
    }
}

