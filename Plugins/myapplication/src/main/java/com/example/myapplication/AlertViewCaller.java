package com.example.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

public class AlertViewCaller
{
    public static final AlertViewCaller _instance = new AlertViewCaller();

    public static AlertViewCaller GetInstance()
    {
        return _instance;
    }

    public static Activity mainActivity;

    public interface AlertViewCallback
    {
        public void onButtonTapped(int id);
    }

    public void ShowAlertView(String[] strings, final AlertViewCallback callbackPositive, final AlertViewCallback callbackNegative)
    {
        if (strings.length<3)
        {
            Log.i("L=>","Error - expected at least 3 strings, got " + strings.length);
            return;
        }
        DialogInterface.OnClickListener myClickListenerPositive = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int id)
            {
                dialogInterface.dismiss();
                Log.i("L=>", "Tapped: " + id);
                callbackPositive.onButtonTapped(id);
            }
        };
        DialogInterface.OnClickListener myClickListenerNegative = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int id)
            {
                dialogInterface.dismiss();
                Log.i("L=>", "Tapped: " + id);
                callbackNegative.onButtonTapped(id);
            }
        };

        AlertDialog alertDialog = new AlertDialog.Builder(mainActivity)
                .setTitle(strings[0])
                .setMessage(strings[1])
                .setCancelable(false)
                .create();
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, strings[2], myClickListenerPositive);
        if(strings.length>3)
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, strings[3], myClickListenerNegative);
        if(strings.length>4)
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, strings[4], myClickListenerPositive);
        alertDialog.show();
    }
}
