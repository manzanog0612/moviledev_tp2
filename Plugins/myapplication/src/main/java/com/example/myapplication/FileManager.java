package com.example.myapplication;

import java.io.BufferedReader;
import java.io.IOException;  // Import the IOException class to handle errors
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.app.Application;
import android.util.Log;

public class FileManager extends Application
{
    public static final FileManager _instance = new FileManager();

    public static FileManager GetInstance()
    {
        return _instance;
    }

    public static Activity mainActivity;
    public static String fileName = "shiptrisLogs.txt";

    public static void WriteToFile(String msg, String path)
    {
        Context context = mainActivity.getApplicationContext();
        try {
                OutputStreamWriter outputStreamWriter= new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_APPEND));

                outputStreamWriter.write(msg + "\n");
                outputStreamWriter.close();

            Log.d("L=>", "Wrote succesfully");
        }
        catch (IOException e) {
            Log.e("L=> Exception", "File write failed: " + e.toString());
        }
    }

    public static String ReadFile(String args)
    {
        Context context = mainActivity.getApplicationContext();

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }

            Log.d("L=>", "Read succesfully");
        }
        catch (FileNotFoundException e) {
            Log.e("L=> login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("L=> login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static void DeleteFile()
    {
        Context context = mainActivity.getApplicationContext();
        context.deleteFile(fileName);
        Log.d("L=>", "Deleted the file: " + fileName);
    }

    public static void ClearFile(String path)
    {
        DeleteFile();
        WriteToFile("", path);
    }
}
